#!/bin/bash
set -e

. /etc/restic-backup-conf.sh

# check if repository needs to be initialized first
if ! restic snapshots 2>&1 >/dev/null; then
    echo "ERROR: no repository found"
    exit 1
fi

# check if repository metadata is ok and verify some of the data blobs
# https://restic.readthedocs.io/en/stable/045_working_with_repos.html#checking-integrity-and-consistency
restic check --read-data-subset=25%

# expire old snapshots:
# - keep the last 12 monthly snapshots
# - keep all snaphots within the last 30 days
# also removes unreferenced data from repo, if at least one snapshot was deleted
# https://restic.readthedocs.io/en/stable/060_forget.html#removing-snapshots-according-to-a-policy
restic forget \
       --keep-monthly 12 \
       --keep-within 30d \
       --prune
