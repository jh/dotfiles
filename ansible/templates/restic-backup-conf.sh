# This file is a sample for the restic backup configuration
# Put this file at `/etc/restic-backup-conf.sh`, make sure it's owned by root and set the permissions to 400
# The following entries are just examples
export RESTIC_REPOSITORY=''
export RESTIC_PASSWORD=''
export B2_ACCOUNT_ID=''
export B2_ACCOUNT_KEY=''
