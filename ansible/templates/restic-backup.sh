#!/bin/bash
set -e

. /etc/restic-backup-conf.sh

# check if repository needs to be initialized first
if ! restic snapshots >/dev/null 2>&1; then
    restic init
fi

# create new backup
# TODO: use variables for these settings
# https://restic.readthedocs.io/en/stable/040_backup.html
restic backup \
       --one-file-system \
       --exclude-caches \
       --exclude='*blog/public/' \
       --exclude='*blog/resources/' \
       --exclude='node_modules' \
       --exclude='.sync_*' \
       --exclude='venv' \
       '/etc' \
       '/root' \
       '/home/jack'

echo "Done. Don't forget to run 'restic-check-prune.sh' once in a while!"

exit 0
