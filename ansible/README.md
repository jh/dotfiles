# Ansible playbooks for setting up the workstation

Run the following command to apply the playbook to the local machine:

```sh
ansible-playbook -K setup.yml
# -C for "check" mode and -D for showing diffs
```

Optionally, choose the tasks you want to run by commenting / uncommenting them from `setup.yml`.
