#!/bin/bash

MUTE=$( awk -F"[][]" '/dB/ { print $6 }' <(amixer sget Master) );

if [[ "$MUTE" == "on" ]]; then
	printf ""; # ON
else
	printf ""; # OFF
fi
