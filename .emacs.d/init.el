;;; init.el --- Initialize Emacs

;;; Commentary:
;; Jack's init.el for Emacs

;;; Code:

;; allow 20MB of memory before calling garbage collection
(setq gc-cons-threshold 20000000)
;; increase the amount of data which Emacs reads from the process to 1MB
(setq read-process-output-max (* 1024 1024))
;; https://emacs-lsp.github.io/lsp-mode/page/performance/

;; disable toolbar, menubar and scrollbar
(menu-bar-mode -1)
(when window-system
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  )

;; don't load outdated byte code
(setq load-prefer-newer t)

;; set theme and font in graphical environment
(when window-system
  (set-frame-font "Hack 11") ;; default: 12

  ;; add custom theme path
  (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

  ;; use aalto light theme (light theme)
  (load-theme 'aalto-light t)

  ;; use seti theme (dark theme)
  ;; (load-theme 'seti t)
  )

;; disable "warnings" from package source code during compilation
(setq native-comp-async-report-warnings-errors nil)

;; set UTF-8 encoding globally
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(setq current-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
(setenv "LC_CTYPE" "UTF-8")

;; set frame title
(setq frame-title-format '("" "emacs: %b (%m)"))

;; custom file for Emacs' custom utility (do no write into my init.el!)
(setq custom-file (concat user-emacs-directory "customize.el"))
(load custom-file t)

;; clear scratch and set it to basic text mode
(setq initial-scratch-message nil)
(setq initial-major-mode 'fundamental-mode)

;; disable backup files
(setq backup-inhibited t)

;; enable overwriting globally
(delete-selection-mode t)

;; disable visual and audio bell
(setq ring-bell-function 'ignore)

;; control TAB
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; Enable indentation+completion using the TAB key.
(setq tab-always-indent 'complete)

;; set default C style
(setq c-default-style "linux"
      c-basic-offset 4)

;; enable case sensitivity in upper-case searches (otherwise case-insensitive)
(setq-default case-fold-search t)

;; auto close brackets (also "", {}, <>, `')
(setq electric-pair-pairs '(
			                (?\" . ?\")
			                (?\{ . ?\})
			                (?\< . ?\>)
                            (?\` . ?\`)
			                ))
(electric-pair-mode 1)

;; highlight matching brackets
(show-paren-mode 1)

;; When opening a file, always follow symlinks
(setq vc-follow-symlinks t)

;; shorten 'yes' and 'no' to 'y' and 'n'
(fset 'yes-or-no-p 'y-or-n-p)

;; Line numbers
(defun enable-line-numbers ()
  "Enable line numbers on the side. Native `line-numbers-mode' on newer Emacs, fallback to `linum-mode'."
  "see https://www.emacswiki.org/emacs/LineNumbers"
  (cond ((>= emacs-major-version 26)
         (display-line-numbers-mode t))
        (t) (linum-mode t))
  ;; (set-face-attribute 'linum nil :foreground "#8b8378")
  )

(defun disable-line-numbers ()
  "Disable line numbers on the side. Either native or `linum-mode'."
  (interactive)
  (when display-line-numbers-mode
    (display-line-numbers-mode nil))
  (when linum-mode
    (linum-mode nil))
  )

;; enable line numbers for appropriate modes (but not comint modes!)
(add-hook 'prog-mode-hook 'enable-line-numbers)
(add-hook 'text-mode-hook 'enable-line-numbers)

;; globally enable winner mode for undoing/redoing window changes
(winner-mode t)

;; auto reload file if changed on disk
(global-auto-revert-mode t)

;; auto reload dired, but be quiet about it
(setq dired-auto-revert-buffer t)
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)

;; if another dired window is open, use that path as default for move/copy/... operations
;; from https://emacs.stackexchange.com/a/5604
(setq dired-dwim-target t)

;; re-use the same buffer when navigating directories with dired (Emacs 28.1+)
(setq dired-kill-when-opening-new-dired-buffer t)

;; hide noisy details by default (shows just filenames)
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(setq dired-hide-details-hide-symlink-targets nil)

;; avoid whitespace-only entries in the kill-ring
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Kill-Options.html
(setq kill-transform-function
      (lambda (string)
        (and (not (string-blank-p string))
             string)))

;; avoid duplicate, subsequent items in the kill-ring
(setq kill-do-not-save-duplicates t)

;; Note: alternatively, consider using https://github.com/NicholasBHubbard/clean-kill-ring.el/blob/main/clean-kill-ring.el

;; Automatically refresh imenu's index
(setq imenu-auto-rescan t)

;; When popping the mark, continue popping until the cursor actually moves
;; from https://endlessparentheses.com/faster-pop-to-mark-command.html
(defadvice pop-to-mark-command
    (around ensure-new-position activate)
  (let ((p (point)))
    (dotimes (i 10)
      (when (= p (point))
        ad-do-it))))

;;  quickly pop the mark several times by typing C-u C-SPC C-SPC, instead of having to type C-u C-SPC C-u C-SPC.
(setq set-mark-command-repeat-pop t)

;; Follow output of compilation buffer (scroll until error)
(setq compilation-scroll-output 'first-error)

;; highlight current line
(global-hl-line-mode 1)

;; delete trailing whitespaces on saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; ensure newline at end of file
(setq require-final-newline t)

;;;;;;;;;;;;;;;;;;;;
;; Spell checking ;;
;;;;;;;;;;;;;;;;;;;;

;; treat CamelCaseSubWords as separate words everywhere
;; (add-hook 'prog-mode-hook 'subword-mode)
(global-subword-mode 1)

;; enable spell checking in plain text mode
(add-hook 'text-mode-hook 'flyspell-mode)

;; disable C-. shortcut
(eval-after-load "flyspell"
  '(define-key flyspell-mode-map (kbd "C-.") nil))

;; tell ispell how to parse LaTeX
(add-hook 'tex-mode-hook
          #'(lambda () (setq ispell-parser 'tex)))
(add-hook 'markdown-mode-hook
          #'(lambda () (setq ispell-parser 'tex)))

;; do not print messages for every word (performance)
;; https://www.emacswiki.org/emacs/FlySpell#Performance
(setq flyspell-issue-message-flag nil)

;; from https://www.emacswiki.org/emacs/FlySpell#Change_dictionaries
(defun fd-switch-dictionary()
  "Switch between german and english dictionary."
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "deutsch8") "english" "deutsch8")))
    (ispell-change-dictionary change)
    (flyspell-buffer)
    (message "Dictionary switched from %s to %s" dic change)
    ))

;; sentence ends with a single whitespace (for M-a and M-e)
(setq sentence-end-double-space nil)

;; when saving a file in a directory that doesn't exist,
;; offer to (recursively) create the file's parent directories
(add-hook 'before-save-hook
	      (lambda ()
	        (when buffer-file-name
	          (let ((dir (file-name-directory buffer-file-name)))
		        (when (and (not (file-exists-p dir))
			               (y-or-n-p (format "Directory %s does not exist. Create it?" dir)))
		          (make-directory dir t))))))


;; from http://whattheemacsd.com/setup-shell.el-01.html
(defun comint-delchar-or-eof-or-kill-buffer (arg)
  "First C-d in shell terminates shell, second C-d kills the buffer."
  (interactive "p")
  (if (null (get-buffer-process (current-buffer)))
      (kill-buffer)
    (comint-delchar-or-maybe-eof arg)))

(add-hook 'shell-mode-hook
          (lambda ()
            (define-key shell-mode-map
                        (kbd "C-d") 'comint-delchar-or-eof-or-kill-buffer)))


;; shell-script-mode has no business messing with our keybindings
(add-hook 'sh-script-mode-hook
          (lambda()
            (local-unset-key (kbd "C-c C-r"))))

;; this should fix garbled output when CLI apps use colors
;; https://wiki.archlinux.org/title/Emacs#Colored_output_issues
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; Bash Automated Testing System
(add-to-list 'auto-mode-alist '("\\.bats\\'" . shell-script-mode))

;; init MELPA archive (external packages from here on)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(require 'package)
(setq package-enable-at-startup nil)
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

;; install use-package if not already present (only required for Emacs < 29)
;; https://github.com/jwiegley/use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  )

(eval-when-compile
  (require 'use-package)
  )
;; always install packages if they are not installed yet
(setq use-package-always-ensure t)
;; For debugging:
                                        ; (setq use-package-verbose t)


;; set default browser
'(browse-url-browser-function (quote browse-url-firefox))

;; when saving a file that starts with `#!', make it executable.
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(defun revert-this-buffer ()
  "Reload current buffer from file (without asking)."
  (interactive)
  (revert-buffer nil t t)
  (message (concat "Reverted buffer " (buffer-name))))

(defun duplicate-line()
  "Duplicate currently active line."
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
  )

;; from http://whattheemacsd.com/file-defuns.el-01.html
(defun rename-current-buffer-file ()
  "Rename current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

;; from http://whattheemacsd.com/file-defuns.el-02.html
(defun delete-current-buffer-file ()
  "Remove file connected to current buffer and kill the buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun prev-window ()
  "Switch to previous window."
  (interactive)
  (other-window -1))

(defun aj-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line."
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))

(defun sudo ()
  "Edit the current buffer as superuser (with TRAMP)."
  (interactive)
  (when buffer-file-name
    (find-alternate-file
     (concat "/sudo:root@localhost:"
             buffer-file-name))))


;; generic key bindings
;; for reference on keybinding:
;; https://www.masteringemacs.org/article/mastering-key-bindings-emacs
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "M-SPC") 'cycle-spacing)
(global-set-key (kbd "C-l") 'comment-or-uncomment-region)
(global-set-key (kbd "C-M-l") 'recenter-top-bottom)
(global-set-key (kbd "C-c +") 'text-scale-increase)
(global-set-key (kbd "C-c -") 'text-scale-decrease)
(global-set-key (kbd "<f5>") 'revert-this-buffer)
;;(global-set-key (kbd "C-c t") 'hs-toggle-hiding)
(global-set-key (kbd "C-c t") 'aj-toggle-fold)
(global-set-key (kbd "C-c d") 'duplicate-line)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-o") 'other-window) ;; other window with M-o, previous with M-- M-o
;; (global-set-key (kbd "M-,") 'prev-window)
(global-set-key (kbd "C-c C-r") 'rename-current-buffer-file)
(global-set-key (kbd "C-c C-d") 'delete-current-buffer-file)
(global-set-key (kbd "<f7>") 'fd-switch-dictionary)
(global-set-key (kbd "M-i") 'imenu)
(global-set-key (kbd "M-z") 'zap-up-to-char) ;; instead of zap-to-char (kills including char)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-g l") 'goto-line)
(global-set-key (kbd "M-/") 'hippie-expand) ;; overwrites dabbrev-expand
(global-set-key (kbd "C-x .") 'endless/ispell-word-then-abbrev) ;; might want to replace flyspell-auto-correct-word (C-.)
(global-set-key (kbd "C-c c") 'calc-grab-region)
(global-set-key (kbd "C-x 9") 'kill-buffer-and-window) ;; and C-x 0 is delete-window
(global-set-key (kbd "C-<tab>") 'switch-to-prev-buffer) ;; switch to previous buffer
(global-set-key (kbd "M-<tab>") 'switch-to-buffer) ;; switch to named buffer via imenu
(global-unset-key (kbd "C-z"))

;; associate major modes with file names
(add-to-list 'auto-mode-alist '("\\.js$'" . js2-mode)) ;; js2-mode or js3-mode
(add-to-list 'auto-mode-alist '("\\.html\$'" . ac-html))

;; extend dired features
(use-package dired-x
  :defer t
  :ensure f ;; this package is included in Emacs
  )

;; helper packages for use-package functionality, see:
;; https://github.com/jwiegley/use-package#diminishing-and-delighting-minor-modes
(use-package diminish)
(use-package bind-key)

;;(diminish 'visual-line-mode)
(global-visual-line-mode 1)

;; ;; custom cheatsheet
;; (use-package cheatsheet
;;   :ensure nil
;;   :defer f
;;   :load-path "elisp/"
;;   :bind ("C-c c" . cheatsheet-show)
;;   :config
;;   (cheatsheet-add-group 'Common
;;                       '(:key "C-h b" :description "Show active Keybindings")
;;                       '(:key "C-q" :description "Quoted Insert")
;;                       '(:key "C-c t" :description "Toggle Hide/Show")
;;                       '(:key "C-c c" :description "Show Cheatsheet")
;;                       '(:key "M-y" :description "Browse Kill Ring")
;;                       '(:key "M-h" :description "Expand Region")
;;                       '(:key "C-c d" :description "Duplicate Line")
;;                       '(:key "M-c" :description "Capitalize following Word")
;;                       '(:key "C-c C-r" :description "Rename file in current Buffer")
;;                       '(:key "C-c C-d" :description "Delete file in current Buffer")
;;                       '(:key "M-SPC" :description "Smartly delete whitespaces")
;;                       '(:key "M-r" :description "Add new Chronos Timer")
;;                       )

;; (cheatsheet-add-group 'Navigation
;;                       '(:key "M-." :description "Next Window")
;;                       '(:key "M-," :description "Previous Window")
;;                       '(:key "C-c left" :description "Undo Window Change")
;;                       '(:key "C-c right" :description "Redo Window Change")
;;                       )


;; (cheatsheet-add-group 'Chronos
;;                       '(:key "a" :description "add timer by specifying expiry time and a message")
;;                       '(:key "SPACE" :description "pause / unpause")
;;                       '(:key "d" :description "deleted timer")
;;                       '(:key "D" :description "deleted all expired timers")
;;                       '(:key "e" :description "edit timer")
;;                       '(:key "l" :description "lap timer")
;;                       '(:key "F" :description "freeze / unfreeze display")
;;                       '(:key "q" :description "quit window")
;;                       )
;;   )


;; (use-package debian-control-mode
;;   :ensure nil
;;   :load-path "elisp/"
;;   :mode ("debian/control\\'")
;;  )

;; (use-package debian-copyright-mode
;;   :ensure nil
;;   :load-path "elisp/"
;;   :mode ("debian/.*copyright\\'" "\\`/usr/share/doc/.*/copyright")
;;   )

;; (use-package debian-changelog-mode
;;   :ensure nil
;;   :load-path "elisp/"
;;   :mode ("debian/*NEWS" "NEWS.Debian" "debian/\\([[:lower:][:digit:]][[:lower:][:digit:].+-]+\\.\\)?changelog\\'" "debian/changelog\\'")
;;   )


;; required packages: editorconfig
(use-package editorconfig
  :diminish editorconfig-mode
  :config
  (add-hook 'prog-mode-hook (editorconfig-mode 1))
  (add-hook 'text-mode-hook (editorconfig-mode 1))
  )


(use-package browse-kill-ring
  :bind ("M-y" . browse-kill-ring)
  :config
  (setq browse-kill-ring-highlight-current-entry t)
  )


(use-package hideshow
  :bind ("C-c t" . hs-toggle-hiding)
  :diminish hs-minor-mode
  :init
  (add-hook 'prog-mode-hook #'hs-minor-mode)
  )

;; When called this automatically detects the submode at the current location.
;; It will then either forward to end of tag(HTML) or end of code block(JS/CSS).
;; This will be passed to hs-minor-mode to properly navigate and hide/show the code.
(defun mhtml-forward (arg)
  (interactive "P")
  (pcase (get-text-property (point) `mhtml-submode)
    (`nil (sgml-skip-tag-forward 1))
    (submode (forward-sexp))))

;; Adds the tag and curly-brace detection to hs-minor-mode for mhtml.
(add-to-list 'hs-special-modes-alist
             '(mhtml-mode
               "{\\|<[^/>]*?"
               "}\\|</[^/>]*[^/]>"
               "<!--"
               mhtml-forward
               nil))

;; (use-package chronos
;;   :ensure nil
;;   :load-path "~/.emacs.d/elisp/"
;;   :bind ("M-r" . chronos-add-timer)
;;   )


;; Required packages: libpng-dev libz-dev libpoppler-glib-dev libpoppler-private-dev
(use-package pdf-tools
  :defer t
  :if window-system
  :mode ("\\.pdf$" . pdf-view-mode)
  :init
  (setq pdf-view-use-unicode-ligther nil)
  (pdf-loader-install)
  :config
  (setq-default pdf-view-display-size 'fit-width)
  )


;; Better flyspell autocorrect
(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("M-TAB" . flyspell-correct-wrapper))
  )
(use-package flyspell-correct-popup
  :after flyspell-correct
  :bind (:map popup-menu-keymap
              ("TAB" . popup-next)
              ("S-TAB" . popup-previous))
  )

(use-package le-thesaurus
  :bind ("C-c s" . le-thesaurus-get-synonyms))

(use-package imenu-anywhere
  :bind ("M-i" . ido-imenu-anywhere)
  )

(use-package exec-path-from-shell
  :defer 3
  :init
  (setq exec-path-from-shell-variables '(
                                         ;; copy SSH agent environment variables into Emacs
                                         "SSH_AGENT_PID"
                                         "SSH_AUTH_SOCK"
                                         ;; copy custom PATH from shell environment
                                         "PATH"
                                         ))
  :config
  (exec-path-from-shell-initialize)
  )

(use-package expand-region
  :bind ("C-=" . er/expand-region)
  )


(use-package multiple-cursors
  :defer t
  :bind (
         ("C-c m l" . mc/edit-lines)
         ("C-c m >" . mc/mark-next-like-this)
         ("C-c m <" . mc/mark-previous-like-this)
         ("C-c m a" . mc/mark-all-like-this)
         ("C-c m d" . mc/mark-all-dwim)
         )
  :init
  ;; make <return> insert a newline; multiple-cursors-mode can still be disabled with "C-g"
  ;; https://old.reddit.com/r/emacs/comments/ry8qfy/multiplecursors_insert_new_line_with_return/
  (with-eval-after-load 'multiple-cursors-core
    (define-key mc/keymap (kbd "<return>") nil)
    )
  :config
  ;; always run commands for all cursors  except for commands that are listed in mc/cmds-to-run-once
  ;; https://github.com/magnars/multiple-cursors.el#unknown-commands
  (setq mc/always-run-for-all 't)
  )


(use-package undo-tree
  :bind ("C-c u" . undo-tree-visualize)
  :diminish undo-tree-mode
  :config
  (setq undo-tree-visualizer-timestamps t)
  (setq undo-tree-visualizer-diff t)
  ;; store undo-tree history in a central location instead of littering them all of the place
  ;; https://github.com/syl20bnr/spacemacs/issues/15426
  (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo-tree-history")))
  ;; enable undo-tree mode everywhere
  (global-undo-tree-mode)
  )


(use-package mood-line
  :init
  (mood-line-mode)
  )

(use-package magit
  :after (project)
  :bind (("<f8>" . magit-status)
         ("C-c g" . magit-file-dispatch) ;; run magit commands on current buffer (file)
         (:map magit-mode-map
               ("C-<tab>" . nil)
               ("M-<tab>" . nil)))
  :config
  (setq magit-diff-refine-hunk t)
  :init
  ;; https://github.com/magit/magit/blob/557ab2c0620930afc0b1da3453c42fa2b5d86145/lisp/magit-extras.el
  (when (boundp 'project-prefix-map)
    ;; Only modify if it hasn't already been modified.
    (equal project-switch-commands
           (eval (car (get 'project-switch-commands 'standard-value))
                 t)))
  (define-key project-prefix-map "m" #'magit-project-status))

(use-package auto-complete
  :defer t
  :diminish auto-complete-mode
  :config
  (ac-config-default)
  )

(use-package flycheck
  :defer t
  :init
  ;; launch flycheck automatically for all programming modes
  (add-hook 'prog-mode-hook 'flycheck-mode)
  )

;;;;;;;;;;;;;;;;;;;;;;;
;; Go configuration: ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; Set up before-save hooks to format buffer and add/delete imports.
;; https://github.com/golang/tools/blob/master/gopls/doc/emacs.md
(defun add-go-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t)
  (add-hook 'before-save-hook #'eglot-code-action-organize-imports t t)
  (add-hook 'before-save-hook #'eglot-format-buffer t t)
  )

(use-package go-mode
  :mode "\\.go\\'"
  :hook (go-mode . add-go-install-save-hooks))

(use-package eglot
  :bind (:map eglot-mode-map
              ("C-?" . eldoc)
              ;; ("C-c C-e" . eglot-rename)
              ;; ("C-c C-o" . python-sort-imports)
              ("C-c ." . eglot-code-action-quickfix)
              )
  ;; Add your programming modes here to automatically start Eglot,
  ;; assuming you have the respective LSP server installed.
  :hook (
         (go-mode . eglot-ensure)
         (web-mode . eglot-ensure)
         (python-mode . eglot-ensure)
         )
  :config
  ;; You can configure additional LSP servers by modifying
  ;; `eglot-server-programs'. The following tells eglot to use TypeScript
  ;; language server when working in `web-mode'.
  (add-to-list 'eglot-server-programs
               '(web-mode . ("typescript-language-server" "--stdio"))
               ))


;; for YAML editing
(use-package yaml-mode
  :mode ("\\.yml\\'" "\\.yaml\\'")
  :config
  (add-hook 'yaml-mode-hook
            (lambda ()
              (flyspell-prog-mode) ;; only spellcheck comments
              (auto-complete-mode) ;; enable auto-complete
              ))
  )

(use-package highlight-indentation
  :config
  (set-face-background 'highlight-indentation-current-column-face "#909093")
  :init
  (add-hook 'yaml-mode-hook 'highlight-indentation-current-column-mode)
  (add-hook 'python-mode-hook 'highlight-indentation-current-column-mode)
  )

(use-package smart-tabs-mode
  :diminish
  :config
  (smart-tabs-mode 1)
  )


(use-package dockerfile-mode
  :mode ("Dockerfile" "Containerfile")
  )


(use-package markdown-mode
  :mode ("\\.md\\'" "\\.markdown\\'")
  )


(use-package smex
  :bind ("M-x" . smex)
  :config
  (setq smex-save-file (concat user-emacs-directory "smex-items"))
  )


(use-package systemd
  :mode (
         ("\\.service*\\'" . systemd-mode)
         ("\\.target*\\'" . systemd-mode)
         ("\\.timer*\\'" . systemd-mode)
         )
  )


;; (use-package groovy-mode
;;   :mode "Jenkinsfile"
;;   )


(use-package json-mode
  :mode "\\.json\\'"
  )


(use-package puppet-mode
  :mode "\\.pp\\'"
  )

(use-package yasnippet
  :defer 2
  :bind ("M-<tab>" . expand-or-tab-properly)
  ;;   (global-set-key (kbd "C-M-i") 'yas-expand)
  :commands (yas-expand)
  :config
  (yas-global-mode 1)

  ;; from https://github.com/magnars/.emacs.d/blob/02b8cf71e54af6b2cc22d789be8a921516857f81/settings/setup-js2-mode.el#L140
  (defun expand-or-tab-properly ()
    "Try to expand YASnippet at current position, if no snippet found, do regular indentation."
    (interactive)
    (let ((yas-fallback-behavior 'return-nil))
      (unless (yas-expand)
        (indent-for-tab-command)
        (if (looking-back "^\s*")
            (back-to-indentation)))))

  )

;; Fix going to the "beginning" (M-<) and "end" (M->) of a buffer
;; for dired, magit and friends
(use-package beginend
  :config
  (beginend-global-mode))


;; Enable ido-mode actually everywhere!
(use-package ido-completing-read+
  :config
  (setq ido-everywhere t
        ido-enable-dot-prefix t
        ido-enable-flex-matching t
        ido-enable-tramp-completion t)
  (ido-mode 1)
  (ido-ubiquitous-mode 1)
  )

;; (use-package kubel
;;   :defer t
;;   :config
;;   (setq kubel-context "default"))

(use-package k8s-mode
  :defer t
  )

;; Enable TRAMP in Docker / Podman containers (only required for emacs 28 and earlier)
;; (use-package docker-tramp
;;   :defer 1
;;   :init
;;   ;; from https://www.emacswiki.org/emacs/TrampAndDocker
;;   (defadvice tramp-completion-handle-file-name-all-completions
;;       (around dotemacs-completion-docker activate)
;;     "(tramp-completion-handle-file-name-all-completions \"\" \"/docker:\" returns
;;     a list of active Docker container names, followed by colons."
;;     (if (equal (ad-get-arg 1) "/docker:")
;;         (let* ((dockernames-raw (shell-command-to-string "podman ps --format '{{.Names}}:'"))
;;                (dockernames (cl-remove-if-not
;;                              #'(lambda (dockerline) (string-match ":$" dockerline))
;;                              (split-string dockernames-raw "\n"))))
;;           (setq ad-return-value dockernames))
;;       ad-do-it))
;;   :config
;;   (setq docker-tramp-docker-executable "podman")
;;   (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;;   )

(use-package avy
  :bind (
         ("C-." . avy-goto-word-1)
         )
  )

;; jump to url and open / copy it
(defun open-or-copy-url ()
  (interactive)
  (save-excursion
    (if (equal current-prefix-arg nil) ; with prefix argument (C-u)
        ;; then
        (call-interactively 'link-hint-open-link)
      ;; else
      (call-interactively 'link-hint-copy-link))
    )
  )
(use-package link-hint
  :defer t
  :bind
  ("C-c o" . open-or-copy-url))

;; auto-format different source code files extremely intelligently: https://github.com/radian-software/apheleia
(use-package apheleia
  :config
  (apheleia-global-mode +1))

;; edit browser textfields with emacs
(use-package atomic-chrome
  :defer 1 ;; delay package loading (and server start) by 1s
  :config
  (setq atomic-chrome-default-major-mode 'markdown-mode)
  (setq atomic-chrome-buffer-open-style 'frame)
  (atomic-chrome-start-server)
  )

;; for profiling init.el startup
;; just run `M-x esup'
(use-package esup
  :defer t
  :config
  ;; Work around a bug where esup tries to step into the byte-compiled
  ;; version of `cl-lib', and fails horribly.
  ;; https://github.com/jschaf/esup/issues/54#issuecomment-651247749
  (setq esup-depth 0)
  )

;; Project management
;; see `C-x p C-h' for help
(use-package project
  :defer 0.5
  :ensure f ;; this package is included in Emacs
  :bind ("<f2>" . project-switch-project)
  :config
  ;; https://github.com/Riyyi/dotfiles/blob/c3811b7f78ade3d7924e46c2b91061bb98208110/.config/emacs/config.org#general-functions
  (defun dot/directory-files-recursively-depth (dir regexp include-directories maxdepth)
    "Depth limited variant of the built-in `directory-files-recursively'."
    (let ((result '())
		  (current-directory-list (directory-files dir t)))
	  (dolist (path current-directory-list)
	    (cond
	     ((and (file-regular-p path)
			   (file-readable-p path)
			   (string-match regexp path))
		  (setq result (cons path result)))
	     ((and (file-directory-p path)
			   (file-readable-p path)
			   (not (string-equal "/.." (substring path -3)))
			   (not (string-equal "/." (substring path -2))))
		  (when (and include-directories
				     (string-match regexp path))
		    (setq result (cons path result)))
		  (when (> maxdepth 1)
            (setq result (append (nreverse (dot/directory-files-recursively-depth
										    path regexp include-directories (- maxdepth 1)))
							     result))))
	     (t)))
	  (reverse result)))

  (defun project-remember-projects-under-recursively (dir maxdepth)
    "Index all projects below directory DIR recursively, until MAXDEPTH."
    (if (file-directory-p dir)
        (let ((files (mapcar 'file-name-directory
					         (dot/directory-files-recursively-depth
						      dir "\\.git$\\|\\.project$" t maxdepth))))
	      (dolist (path files)
	        (project-remember-projects-under path)))))

  (project-remember-projects-under-recursively "~/git/" 2)
  (project-remember-projects-under-recursively "~/cern/" 4)

  )

;; TypeScript, JS, and JSX/TSX support.
(use-package web-mode
  :defer t
  :mode (("\\.ts\\'" . web-mode)
         ("\\.js\\'" . web-mode)
         ("\\.mjs\\'" . web-mode)
         ("\\.tsx\\'" . web-mode)
         ("\\.jsx\\'" . web-mode))
  :custom
  (web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))
  (web-mode-code-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-enable-auto-quoting nil)
  :config
  (add-hook 'web-mode-hook
            (lambda ()
              (when (string-equal "tsx" (file-name-extension buffer-file-name))
                (setup-tide-mode))))
  )

(use-package tide
  :after web-mode
  :init
  ;; formats the buffer before saving
  (add-hook 'before-save-hook 'tide-format-before-save)
  (add-hook 'typescript-mode-hook #'setup-tide-mode)
  :config
  ;; enable typescript - tslint checker
  (flycheck-add-mode 'typescript-tslint 'web-mode)
  )

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  ;; (company-mode +1))
  )

(provide 'init)
;;;  Init.el ends here
