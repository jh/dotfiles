# Emacs

## Useful Emacs commands

* `re-builder` - interactive RegEx tool
* `vc-region-history` - show history of region (inside version control)
* `emacs-init-time` - display Emacs launch time
