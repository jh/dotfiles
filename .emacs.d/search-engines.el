;;; search-engines.el --- Sets up engine-mode

;;; Commentary:
;; Sets up engine-mode
;; compile with: M-x byte-compile-file
;; load with: (load-file "~/.emacs.d/search-engines.elc")


;;; Code:
(require 'engine-mode)

(defengine duckduckgo
  "https://duckduckgo.com/?q=%s"
  :keybinding "d")

(defengine github
  "https://github.com/search?ref=simplesearch&q=%s"
  :keybinding "g")

(defengine openstreetmap
  "https://www.openstreetmap.org/search?query=%s"
  :keybinding "o")

(defengine stack-overflow
  "https://stackoverflow.com/search?q=%s"
  :keybinding "s")

(defengine wikipedia
  "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"
  :keybinding "w")

(defengine wiktionary
  "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s")

(defengine wolfram-alpha
  "http://www.wolframalpha.com/input/?i=%s"
  :keybinding "a")

(defengine youtube
  "http://www.youtube.com/results?aq=f&oq=&search_query=%s"
  :keybinding "y")

;; enable engine minor mode
(engine-mode t)

(provide 'search-engines)
;;;  search-engines.el ends here
