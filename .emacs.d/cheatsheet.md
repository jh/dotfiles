
Universal negative argument: `C--` or `M--`
Universal numeric argument: `C-N` or `M-N` (with N being a number)

##  Movement

Key Sequence | Action | Notes
------------|--------------------|-------
`M-a`,  `M-e`            | `backward-sentence` / `forward-sentence`
`C-M-f`                  | Move forward by s-expression (`forward-sexp`)
`C-M-b`                  | `backward-sexp`
`C-M-d`                  | Move down in list of sexp
`C-M-u`                  | Move up in list of sexp
`C-M-k`                  | Kill sexp at point
`C-M-n`                  | Move to next item in list of sexp
`C-M-p`                  | Move to previous item in list of sexp
`M-{`, `M-}`             | Move to beginning / end of paragraph (rebind to M-[] ?)
`C-M-a`, `C-M-e`         | Move to beginning / end of defun
`M-m`                    | Move to "start" of a line (`back-to-indentation`)
`C-M-v`                  | Scroll up the other window
`C-M-S-v`, `C-M-- C-M-v` | Scroll down the other window
`M-r`                    | Reposition cursor to middle, top, bottom of screen | Cycles
`C-M-l`                  | Recenters the point to middle, top, bottom of buffer | Cycles
`M-g l`                  | Go to line | Originally `M-g g`
`M-g c`                  | Go to character (absolute in buffer)
`M-g TAB`                | Go to column | column i.e. character in current line
`M-.`                    | Go to definition (dumb-jump / lsp)
`C-.`                    | Jump to word (avy-goto-word-1)
`C-u C-.`                | Jump to char (aby-goto-char)
`C-c M-o`                | Clear Comint buffer (`comint-clear-buffer`)

Unbound: `M-'`

## Marking

The direction of all these commands can be inverted with the universal negative argument and a universal numeric argument will mark multiple items.

`C-x h`     | Mark the whole buffer
`M-h`       | Mark next paragraph
`C-M-h`     | Mark next defun
`C-M-Space` | Mark next sexp
`M-@`       | Mark next word
`C-=`       | Expand region
`C-u Space` | Jump to last mark
`C-x C-x`   | Exchange point and mark and reactive last region

## Editing

Remember universal numeric and negative arguments!

`M-d`, `C-Backspace`                 | Kill word
`C-k`                                | Kill rest of line
`M-k`                                | Kill sentence
`C-M-k`                              | Kill sexp
`C-S-Backspace`                      | Kill current line | Rebind to C-S-k
`C-M-t`                              | `transpose-sexps`
`C-x C-t`                            | `transpose-lines`
``                                   | `transpose-paragrahps`, `transpose-sentences`
`M-c`                                | Capitalize next word | Only first character
`M-u`                                | Uppercase next word | All characters
`M-l`                                | Lowercase next word | All characters
`M-=`                                | Count words, lines and characters (`count-words`)
`C-o`                                | Insert new line (without moving point)
`C-x C-o`                            | Delete all subsequent blank lines
`M-^`                                | Join current onto previous line
`M-/`                                | Expand word (with DAbbrev or Hippie)
`C-x TAB`                            | `indent-rigidly` | Negative and numeric arguments!
`M-z`                                | `zap-to-char` | Negative argument!
`C-.`, `M-TAB`                       | Auto correct word at point (Cycles through options) | flyspell-mode only
`C-,`                                | Go to next mispelled word | flyspell-mode only
`M-x ispell-buffer`, `ispell-region`, `flyspell-buffer` | Run spell check for buffer / region
`M-x read-only-mode`                 | Toggles read only mode for buffer
`M-x sudo`                           | Edit current file as su (with TRAMP) | Custom function
`M-x highlight-` `phrase`, `regexp`, `symbol-at-point` | Highlights a pattern
`M-x keep-lines`, `flush-lines` | `delete-non-matching-lines` / `delete-matching-lines` according to regex

## Bookmarks

Bookmarks also work across various special modes such as TRAMP.

`C-x r m` | Set new bookmark
`C-x r l` | List bookmarks
`C-x r b` | Jump to bookmark

## Isearch

`M-n`, `M-p` | Move to next / previous item in search history
`C-M-i`      | Auto-complete string from search history
`M-s w`      | Toggle word mode | Will ignore punctuation and delimiters while searching
`M-s Space`  | Toggle lax whitespace matching

## Occur

Occur mode creates a new buffer from lines in the current buffer matching a given pattern (like grep).
This new buffer can then be modified and the changed lines can be written back to the original buffer.

`M-s o`      | `occur-mode`
`M-n`, `M-p` | Go to next / previous occurrence
`<`, `>`     | Go to beginning / end of occur buffer
`g`          | Refresh search results
`q`          | Quit occur mode (discards buffer)
`e`          | Enable edit mode
`C-c C-c`    | Exit occur mode and apply changes to original buffer

## EWW

`M-x eww`          | Open EWW
`C-u M-x eww`      | Open new EWW buffer
`TAB`, `S-TAB`     | Go to next / previous hyperlink
`l`, `r`           | Go backward / forward in browsing history
`p`, `n`, `u`, `t` | Semantic navigation (if supported by web page)
`R`                | Switch to reader mode
`&`                | Open current page with browse-url
`C-u RET`          | Open link at point in external browser
`M-RET`            | Open link in new EWW buffer
`w`                | Copy link at point
`M-s M-w`          | Search for point / region on the internet
`q`                | Quit EWW
`B`                | Show bookmarks
`b`                | Add bookmark

## Dired

`^`           | Goes up one directory (`..`)
`q`           | Quit this dired buffer
`m`, `u`, `t` | Mark active / unactive / toggle
`U`           | Unmark everything
`d`           | Flag for deletion
`(`           | Show / Hide file details
`v`           | Open file in `view-mode` (transient and read-only)

Region marking:
`* m`, `* u`  | Mark / unmark region
`* %`         | Mark files by regexp
`* .`         | Mark files by extension
`* c`         | Change mark
`* t`         | Toggle (invert) marks

Operations:
`C`           | Copy files
`R`           | Rename or move files
`S`, `Y`      | Create absolute / relative symlink to marked file
`O`, `G`, `M` | Change owner / group / permissions
`D`           | Deletes marked files | files with "*"
`x`           | Deletes flagged files | files with "D"
`F`           | Visit files | Opens a buffer for each marked file
`c`           | Compress marked files into archive
`z`, `Z`      | Create Zip archive / Unzip archive
`g`           | Refresh buffer
`+`           | Create subdirectory
`s`           | Toggles sorting by name / date
`<`, `>`      | Jump to previous / next directory
`j`           | Jump to file
`!`           | Synchronous shell command | use argument "*" to have all files in one command, "?" for one command per file
`&`           | Asynchronous shell comamnd
`i`           | Insert subdirectory | Shows another directory in the current dired buffer

## Shell mode

(Or any `comint-mode`-based mode, really.)

`C-c C-p` / `C-c c-n` | Go to previous / next command line
`M-p` / `M-n` | Go to previous / next history item
`C-c C-u` | Delete the current input string backwards up to the cursor
`C-c C-x` | Repeat previous commands (in order)
`C-c .` | Insert last argument of previous command
`C-c C-l` | Show command history

See also: <https://www.masteringemacs.org/article/shell-comint-secrets-history-commands>

## Shell commands

`M-!`     | Run shell command (`shell-command`)
`C-u M-!` | Run shell command and insert output into buffer
`M-|`     | Pipe region into shell command (`shell-on-region`)

## Project navigation

https://www.gnu.org/software/emacs/manual/html_node/emacs/Project-File-Commands.html

`C-x p p` / `F2` | Switch project
`C-x p f` | Find file in current project
`C-x p b` | Switch buffer in project
`C-x p D` | Open project root in dired
<!-- `C-c p q` | Switch between open projects -->
`C-x p g` | Search for text (grep) in project
`C-x p r` | Replace text in project
`C-x p s` | Spawn a new shell-mode buffer for the project
`C-x p !` | Run a shell command in the root of the project (use `&` for an async command)
`C-x p k` | Close project (kills all associated buffers)
`C-x p C-h` | Help yourself (view available keybindings)

## Misc.

`M-x lgrep` | grep files specified by glob
`M-x rgrep` | recursively greps in specified files
`C-c +`     | Increase font size in buffer (`text-scale-increase`)
`C-c -`     | Decrease font size in buffer (`text-scale-decrease`)
`C-c g`     | Run magit commands on current buffer (file)
`M-x artist-mode` | Use the mouse to draw lines, rectangles and ellipses
`M-x follow-mode` | Shows a contiguous buffer paginated across multiple windows
`M-x htmlfontify-buffer` | Reproduce the look the buffer as an HTML file with CSS
`C-c s` | Search synonyms for word at the cursor

## Helping yourself

`M-x info-apropros`, `apropros-command` | Help for specific topics and commands
`(prefix) C-h` | List all keybindings which start with "prefix"
`C-h k` | Describe what a key does
`C-h f` | Describe what a command does
`C-h m` | Describe current mode(s)
