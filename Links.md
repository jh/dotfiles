# Links
Links to various tools, programs, websites and more.


## Programs

### GUI
* [Zeal](https://zealdocs.org/) - Offline API documentation

### Console
* [ncdu](https://dev.yorhel.nl/ncdu) - ncurses based front end to `du`


## Websites
* [Mailinator](https://mailinator.com) - Throwaway Mail Addresses
* [geek_typer](http://geektyper.com) - Look like a real hacker!
* [SuperCook](http://www.supercook.com) - Search for recipes by your ingedrients
* [Symbolab](https://www.symbolab.com/) - Step-by-step Math solver
* [SharkLasers](https://www.sharklasers.com/) - Disposable Temporary E-Mail Address
* [10minutemail](https://10minutemail.com/) - 10 minute temporary mail
* [Zamzar](http://www.zamzar.com/) - Online conversion tool
* [Roadtrippers](https://roadtrippers.com) - Plan a road trip with land marks, POI, food, etc.
* https://www.mathway.com/ - Math Solver
* [Algorithms and Data Strutures](https://xlinux.nist.gov/dads/) - NIST's Dictionary of Algorithms and Datastructures
* [Keepa](https://keepa.com/) - Amazon Price Tracker
* [Awesome Lists](https://github.com/sindresorhus/awesome)
* [Hackr.io](https://hackr.io/) - List of online course & tutorials for any programming language
* http://comnuan.com/ - Online Numerical Calculators (Matrix, Integer and Fractions, Calculus)
* [https://forthebadge.com/](For the badge) - Badges for the badges' sake
* [https://shields.io/](ShieldsIO) - Status badges
* http://www.stream-urls.de - Direct links to various german radio stations

### Background Sounds
* [Noisli](https://www.noisli.com/) - Background Noise Generator
* [Rainy Mood](http://www.rainymood.com/) - Rain & Storm Background Noise
* [Now Relax](http://www.rainymood.com/) - Relaxing Background Noise
* [Jazz and Rain](http://www.jazzandrain.com/) - Relaxing Jazz and soothing Rain
* [Epic Music Time](http://epicmusictime.com/) - Epic feel good music


### Cheat Sheets / Lists
* [Emoji Codes](https://emoji.codes/)
* [Awesome Lists](https://github.com/sindresorhus/awesome)


## Go
Some useful (or not so) Go tools and libraries

### Development
* [Go Cheat Sheet](https://github.com/a8m/go-lang-cheat-sheet)
* [Go Patterns](https://github.com/tmrts/go-patterns) - Design patterns, recipes and idioms for Go
* [hellogopher](https://github.com/cloudflare/hellogopher) - Makefile for Golang projects ([Example](https://github.com/FiloSottile/zcash-mini))

### Tools
* [httplab](https://github.com/gchaincl/httplab)
* [wuzz](https://github.com/asciimoo/wuzz)
* [rclone](https://github.com/ncw/rclone)
* [gocolor](https://github.com/songgao/colorgo) - Colorized output for `go build` and `go test`
* [gocode](https://github.com/nsf/gocode) - Go autocompletion daemon

### Libraries
* https://github.com/alexflint/go-arg
* https://github.com/mattn/go-xmpp
* https://github.com/go-sql-driver/mysql
* https://github.com/mattn/go-sqlite3
* https://github.com/bclicn/color

### Resources
* [awesome-go](https://github.com/avelino/awesome-go)
* [go-hardware](https://github.com/rakyll/go-hardware)


## Web Development
HTML / CSS / JavaScript

### CSS Frameworks
* [Picnic CSS](https://picnicss.com) - Lightweight and beautiful library
* [Milligram](http://milligram.io/) - A minimalist CSS framework
* [PureCSS](https://purecss.io/) - Lightweight, nice package

## Color Palettes
* http://clrs.cc/ - A nicer color palette for the web

## Icons
* [Fontello](http://fontello.com/) - Icon Font Generator
* [Noun Project](https://thenounproject.com/) - Icons for everything
