#!/bin/sh

ffmpeg -i "$1" -c:v copy -c:a aac -strict experimental -b:a 128k "$(basename $1 .MTS).mp4"
