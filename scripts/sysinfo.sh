#!/bin/bash

# RAM Usage
RAM=$(free -mot | tail -n 1 - | tr -s ' ');
TOTAL_RAM=$( echo $RAM | cut -d' ' -f2,2 -);
USED_RAM=$(echo $RAM | cut -d' ' -f3,3 -);
#FREE_RAM=$(echo $RAM | cut -d' ' -f4,4 -);
echo "RAM: ${USED_RAM}MB / ${TOTAL_RAM}MB";

# Board Temp
echo -n "Board Temperature: ";
cat /sys/devices/platform/sunxi-i2c.0/i2c-0/0-0034/temp1_input | awk '{ printf ("%0.1f°C\n",$1/1000); }'

# Load

# HDD Temp

# CPU Clock

# Memory usage NAND
echo "NAND usage: $(df -h --output='pcent' / | tail -1)";

# Memory usage HDD
echo "HDD usage: $(df -h --output='pcent' /mnt/hdd | tail -1)";

# Uptime
echo "Uptime: $(better-uptime)";

# OS
if [ -e "/etc/os-release" ]; then 
	echo -n "OS: "
	cat /etc/os-release | head -1 | cut -d'"' -f2; 
fi;

# Kernel
echo "Kernel-Version: $(uname -r)";

# Last apt-get update
echo -n "Last 'apt-get update': ";
echo $(date -d @"$(stat -c %Y /var/lib/apt/periodic/update-success-stamp)" +'%Y-%m-%d at %H:%M');
