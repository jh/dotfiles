#!/bin/bash

HOSTNAME=${1:-$(hostname)};

case "$HOSTNAME" in
    DebianDesktop)
        compton -b
        blueman-applet &
        "${HOME}/dotfiles/scripts/xkbd.sh" 'us';
        ;;

    ZenBook)
        nm-applet --indicator &
        blueman-applet &
        nextcloud --background &
        ;;

    infinitybook-s14)
        /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
        nm-applet --indicator &
        blueman-applet &
        nextcloud --background &
       # tuxedo-control-center &
        ;;
    jh-elitebook)
        /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
        nm-applet --indicator &
        mattermost-desktop &
        cernbox &
        ;;
esac

exit 0;
