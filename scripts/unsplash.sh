#!/bin/bash

# Downloads my favorite wallpapers from UnSplash.com

DIR="${HOME}/dotfiles/unsplash/"
BASE='https://images.unsplash.com/'

mkdir -p "$DIR"

WALLPAPERS=(
    'photo-1447877085163-3cce903855cd'
    'photo-1439792675105-701e6a4ab6f0'
    'photo-1450849608880-6f787542c88a'
    'photo-1443890923422-7819ed4101c0'
    'photo-1439390916221-371302a6218d'
    'photo-1439694458393-78ecf14da7f9'
    'photo-1434873740857-1bc5653afda8'
    'photo-1434064511983-18c6dae20ed5'
    'photo-1436891620584-47fd0e565afb'
    'photo-1442551382982-e59a4efb3c86'
    'photo-1464822759023-fed622ff2c3b'
    'photo-1464822759023-fed622ff2c3b'
    'photo-1452711932549-e7ea7f129399'
    'photo-1464054313797-e27fb58e90a9'
    'photo-1475139475866-6fef2ef89731'
    'photo-1452570053594-1b985d6ea890'
    'photo-1469173479606-ada03df615ac'
);

if [ ! $( command -v curl >/dev/null 2>&1 ) ]; then
    DL='curl -# -o'
elif [ ! $( command -v wget >/dev/null 2>&1 ) ]; then
    DL='wget -q --show-progress --progress=bar -O'
else
    echo "Error: Neither 'curl' nor 'wget' found."
    exit 1
fi


for e in "${WALLPAPERS[@]}"; do
    if [ -f "${DIR}${e}.jpg" ]; then
	echo "File present"
    else
	$DL "${DIR}${e}.jpg" "${BASE}${e}"
    fi
done

echo "Done."
exit 0
