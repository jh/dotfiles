#!/bin/bash
# Print various colors in Bash
for code in {0..255}; do
        if [[ code%8 -eq 0 ]];then echo ;fi
        printf "%5s" `echo -n -e " \e[38;05;${code}m ${code}: Test;"`
done

echo ''
