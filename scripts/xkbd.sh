#!/bin/bash
# Sets the appropriate keyboard layout and variant
# Optionally prints help

_us() {
    if [[ "$1" == 'help' ]]; then
	_us_help;
    else
	setxkbmap -layout 'us' -variant 'altgr-intl';
    fi
}

_us_help() {
    echo "
 Ä = AltGr-Q
 Ö = AltGr-P
 Ü = AltGr-Y
 ß = AltGr-s
 § = AltGr-S
 µ = AltGr-m
 € = AltGr-5
 ’ = AltGr-0
 ´ = AltGr-'
 ° = AltGr-;
 ø = AltGr-L
 ¬ = AltGr-\\
 « = AltGr-[
 » = AltGr-]
";
    
}

_de() {
    if [[ "$1" == 'help' ]]; then
	_de_help;
    else
	setxkbmap -layout 'de';
    fi
}

_de_help() {
    echo 'Not implemented';
}

case "$1" in
    "us")
	_us "$2";
	 ;;
    "de")
	_de "$2";
	;;
    *)
	echo 'Not implemented';
	;;
esac
