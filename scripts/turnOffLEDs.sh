#!/bin/bash
# Script turn off the blue, orange, green and white LEDs on the Cubietruck

echo 0 > /sys/class/leds/blue\:ph21\:led1/brightness
echo 0 > /sys/class/leds/green\:ph07\:led4/brightness
echo 0 > /sys/class/leds/orange\:ph20\:led2/brightness
echo 0 > /sys/class/leds/white\:ph11\:led3/brightness


