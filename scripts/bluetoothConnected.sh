#!/bin/bash

devices="$(bluetoothctl <<< devices | grep ^Device)"

while read -r dev; do
    id="$(echo $dev | cut -d' ' -f2 | tr -d [:space:])"
    name="$(echo $dev | cut -d' ' -f3 | tr -d [:space:])"
    if bluetoothctl <<< info $id | grep 'Connected: yes'; then
#        bluetoothctl <<< info $id
        echo "Device $name ($id) is connected"
    fi
done <<< "$devices"
