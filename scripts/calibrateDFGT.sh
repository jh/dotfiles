#!/bin/bash
# Calibrate your Logitech Driving Force GT on Linux with 'ltwheelconf'
# to support 900 degrees and proper force feedback (FFB)
sudo ltwheelconf -w DFGT --nativemode --range 900 --autocenter 100 --rampspeed 1

