#!/bin/bash
# Flushes Linux Kernel's internal buffers and (file) caches
sudo sh -c "sync; echo 3 > /proc/sys/vm/drop_caches";
