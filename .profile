# Set Editors
export ALTERNATE_EDITOR='nano'
export EDITOR='emacs -nw -Q'
export VISUAL='emacs -Q'
export GIT_EDITOR='nano'
export LESS='-i --QUIET --RAW-CONTROL-CHARS'
# do not paginate if content fits on a page
export LESS="-F -X $LESS"

# Set Language / Locale
export LANG='en_US.UTF-8'
export LANGUAGE='en_US.UTF-8:en'

# Debian Stuff
export DEBEMAIL='jackdev@mailbox.org'
export DEBFULLNAME='Jack Henschel'
export DEBNAME=$DEBFULLNAME

# Programming
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
# No more GOPATH! Well, it's still there, but no need to mess with it anymore
# export GOPATH="$HOME/go"
export PATH="$PATH:$HOME/go/bin"

# Window system
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
