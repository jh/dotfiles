# Build:
# $ docker build -t debian-devel -f debian-devel.Dockerfile .
# Run:
# docker run --name deb-dev --hostname deb-dev -it debian-devel

FROM debian:testing
MAINTAINER Jack Henschel <jh@openmailbox.org>

# Add debian sources repository to the /etc/apt/sources.list file
RUN sed -i \
  '/testing[[:space:]]\+main[[:space:]]*$/\
  {p; s/^\([[:space:]]*\)deb\([[:space:]]\+.*\)$/\1deb-src\2/}' \
  /etc/apt/sources.list

# Install debian development and packaging tools
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf \
    automake \
    build-essential \
    cmake \
    debhelper \
    devscripts \
    dh-make \
    dh-make-golang \
    dh-systemd \
    dpatch \
    emacs-nox \
    equivs \
    fakeroot \
    git \
    git-buildpackage \
    lintian \
    locales \
    make \
    nano \
    ncurses-term \
    pristine-tar \
    quilt \
    sudo


# Add debian user
RUN useradd \
  --groups=sudo \
  --create-home \
  --home-dir=/home/debian \
  --shell=/bin/bash \
  debian

# Disable sudo password checking for users of the sudo group
RUN sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers

# Clean image
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir /var/lib/apt/lists/partial

# Setup environment
ENV DEBFULLNAME='Jack Henschel' \
    DEBEMAIL='jh@openmailbox.org'
USER debian
WORKDIR /home/debian
COPY Debian-Packaging.md /home/debian/

RUN git clone 'https://git.cubieserver.de/jh/dotfiles.git' && \
    rm -f '.bashrc' && ln -s 'dotfiles/.bashrc' && \
    ln -s 'dotfiles/.gitignore_global' && \
    ln -s 'dotfiles/.gitconfig' && \
    ln -s 'dotfiles/.nanorc' && \
    ln -s 'dotfiles/.nano/' && \
    ln -s 'dotfiles/.gbp.conf' && \
    ln -s 'dotfiles/.emacs.d/' && \
    mkdir -p '.config' && cd '.config' && \
    ln -s '../dotfiles/linitian/'

# Default shell
CMD ["/bin/bash","--login","-i"]
