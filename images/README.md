# Images

## Debian Infinite
`debian-infinite.png`by Ozgu Ozden, License GPL-2.0+ (https://wiki.debian.org/DebianArt/Themes/Infinite)

## Simple GNU/Linux Wallpaper
`gnu+linux.png` by [Dablim](https://dablim.devianart.com), License [CC-BY](https://creativecommons.org/licenses/by/3.0/) (https://dablim.deviantart.com/art/Simple-GNU-Linux-Wallpaper-336558602)

## The GNU Ocean
No attribution at hand, sorry :-(

## Unsplash
Thanks to [UnSplash](https://unsplash.com/) for providing (see unsplash.sh script) awesome pictures for [free](https://unsplash.com/license).
