# Debian Packaging
My personal set of reminders for Debian packaging

## Build package (above debian/) with git-buildpackage (gbp)
`gbp buildpackage`

 - `-us -uc` without signing (passed to dpkg-buildpackage(1))
 - `--git-ignore-new` ignore uncommited changes
 - `--git-debian-branch=branchname` working branch
 
## Patching (quilt)
Create new patch: `quilt new patchanme`

Register file for patching: `quilt add filename`

(Make changes to the file)

Refresh: `quilt refresh`

Remove (unapply) changes from source again `quilt pop`

 
## Testing / Checking
`linitan package.changes`
`check-all-the-things`

## Upload
`dput mentors package.changes`
