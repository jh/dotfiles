# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Launch Wayland / Sway if binary present, not already running and virtual terminal == 1
SWAY_BIN="/usr/bin/sway"
if [[ -x "$SWAY_BIN" ]] && [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    # natively use wayland for (most) applications
    # might need to unset some of the env vars for specific applications
    export XDG_CURRENT_DESKTOP=Unity
    export GDK_BACKEND=wayland
    export CLUTTER_BACKEND=wayland
    exec "$SWAY_BIN"
fi

# Launch X display server if not already running, virtual terminal == 1 and startx exists
# if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ] && [ -x "$(which startx)" ]; then
#     exec startx
# fi

# Bash History: ignore duplicates and lines beginning spaces
shopt -s histappend
HISTFILESIZE=10000
HISTSIZE=1000
HISTFILE=~/.bash_history
# ignoreboth == ignoredups,ignorespaces
HISTCONTROL=ignoreboth

# Disable Bell (and discard any output)
xset -b b off > /dev/null 2>&1

# check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize
# command name that is the name of a directory is executed as if it were the argument to the cd command
shopt -s autocd
# correct minor errors in directory component of cd command
shopt -s cdspell
# list status of all jobs before exiting interactive shell
shopt -s checkjobs
# save multiple-line commands as single lines in history
shopt -s cmdhist
# correct spelling mistakes to match a glob
shopt -s dirspell
# enable recursive globbing with **
shopt -s globstar
# case-insensitive pathname expansion
shopt -s nocaseglob

# Enable bracketed paste mode
bind 'set enable-bracketed-paste on'

# Launch SSH-Agent (from http://mah.everybody.org/docs/ssh)
SSH_ENV="$HOME/.ssh/environment"

function start_ssh_agent {
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    chmod 600 "${SSH_ENV}"
    # source (make environment variables available to the shell)
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        echo "Failed to add SSH identity (exit code ${EXIT_CODE})"
    fi
}

# Source SSH settings, if applicable
if [ -d "$HOME/.ssh" ]; then
    if [ -f "${SSH_ENV}" ]; then
        . "${SSH_ENV}" > /dev/null
        ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_ssh_agent;
        }
    else
        start_ssh_agent;
    fi
fi

# Colorize man pages
alias man="LESS_TERMCAP_mb=$'\x1B[1;31m' \
          LESS_TERMCAP_md=$'\x1B[1;31m' \
          LESS_TERMCAP_me=$'\x1B[0m' \
          LESS_TERMCAP_se=$'\x1B[0m' \
          LESS_TERMCAP_so=$'\x1B[1;44;33m' \
          LESS_TERMCAP_ue=$'\x1B[0m' \
          LESS_TERMCAP_us=$'\x1B[1;32m' \
          man"

# Read Markdown files like man pages
md() { pandoc -s -f markdown -t man "$*" | man -l -; }

# Quickly create / remove a directory in tmpfs
mktmp() { mkdir $@; sudo mount -t 'tmpfs' 'tmpfs' $@; }
rmtmp() { sudo umount $@; rm -r $@; }

open() { xdg-open "$@" 2>&1 > /dev/null & }

# enable programmable completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

alias k='kubectl'

source "$HOME/.aliases"
# make sure to include latest version of .profile (usually only sourced during login)
if [ -f "$HOME/.profile" ]; then
  . "$HOME/.profile"
fi
