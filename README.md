# Dotfiles

## Setup with Ansible

The dotfiles, configuration, packages and services can be (partially) deployed with Ansible.
See the `ansible/README.md` for details.

## Hotkeys

* <kbd>Super+`</kbd>: Hide last notification (mako)
* <kbd>Super+~</kbd>: Show last notification (mako)
* <kbd>Super+Pause<kbd>: Open system control menu (poweroff, reboot, sleep etc.)
* <kbd>Super+d<kbd>: Open application launcher
* <kdb>Super+q<kbd>: Close application
* <kbd>Super+Shift+Q<kbd>: Kill application

## HTTP Server
Create a simple HTTP server in the current directory with python:
```
# python3 -m http.server
```

## Debian
Show all packages from 'contrib' and 'non-free'

```
dpkg-query -W --showformat='${Package}\t${Section}\n' |grep -e non-free -e contrib
```

## tar
Because I can never remember how to do this:
```
$ tar -czvf [OUTPUT.tar.gz] [ITEMS...]
$ tar -czvf - [ITEMS...] | pigz --best > [OUTPUT.tgz] # multi-threaded
```

## ffmpeg: Create Slideshow from Images
see https://trac.ffmpeg.org/wiki/Slideshow

MP4:
```
cat *.JPG | ffmpeg -framerate 24 -f image2pipe -i - -c:v libx264 -pix_fmt yuv420p out.mp4
```

WebM:
```
cat *.JPG | ffmpeg -framerate 24 -f image2pipe -i - -vf scale=1920:-1 -c:v libvpx-vp9 -crf 30 -b:v 0  -pix_fmt yuv420p out.webm
```

## Default Applications

Set default web-browser in Debian:
```
$ update-alternatives --config x-www-browser
```

Set detault web-browser in Arch Linux / Manjaro:
```
$ xdg-settings set default-web-browser firefoxdeveloperedition.desktop
# xdg-settings get default-web-browser
```

## Rotate PDF pages

```sh
# 90: counter-clockwise, 270: clockwise
$ pdfjam --landscape --angle 90 pdf-input.pdf
# output will be written to pdf-input-pdfjam.pdf
```

https://unix.stackexchange.com/a/459596

## List orphaned packages

```sh
pacman -Qdt
```

https://bbs.archlinux.org/viewtopic.php?id=57431

## Remove package including dependencies

```sh
pacman -Rs <package>
```

## Combine PDFs

(lossless)

```sh
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=out.pdf in1.pdf in2.pdf
```
